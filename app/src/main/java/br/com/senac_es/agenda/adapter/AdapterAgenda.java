package br.com.senac_es.agenda.adapter;


import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.senac_es.agenda.R;
import br.com.senac_es.agenda.model.Agenda;

public class AdapterAgenda extends BaseAdapter{

    private List<Agenda> lista ;
    private Activity contexto  ;


    public AdapterAgenda(Activity contexto, List<Agenda> lista){

        this.contexto = contexto;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int idince) {
        return this.lista.get(idince);
    }

    @Override
    public long getItemId(int id) {
        int posicao  = 0 ;

        for (int i = 0 ; i < this.lista.size() ; i++){
            if(this.lista.get(i).getId() == id){
                posicao = i ;
                break;
            }
        }

        return posicao;

    }

    @Override
    public View getView(int posicao, View cnvertview, ViewGroup parent) {

        View view = contexto.getLayoutInflater().inflate(R.layout.item_lista_contato , parent , false);

        ImageView imageView = view.findViewById(R.id.imageViewtxt) ;
        TextView textViewNome = view.findViewById(R.id.txtnome) ;
        Agenda agenda = this.lista.get(posicao) ;

        // imageView.setImageBitmap(cachoeira.getImagem());
        textViewNome.setText(agenda.getNome());


        return view;


    }
}
