package br.com.senac_es.agenda;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.senac_es.agenda.model.Agenda;

public class MainApresentActivity extends AppCompatActivity {

    private Agenda agenda;


    public void incApresnt() {
        TextView txtnome = findViewById(R.id.txtnome);
        TextView txttelefone = findViewById(R.id.txtelefone);
        TextView txtcelular = findViewById(R.id.txtcelular);
        TextView txtendereco = findViewById(R.id.txtendereco);
        ImageView imageView = findViewById(R.id.imageViewtxt);

        txtnome.setText(agenda.getNome());
        txttelefone.setText(agenda.getTelefone());
        txtcelular.setText(agenda.getCelular());
        txtendereco.setText(agenda.getEndereco());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_apresent);

        Intent intent = getIntent();

        agenda = (Agenda) intent.getSerializableExtra(MainActivity.AGENDA);

        if(agenda != null){
            incApresnt();
            Bitmap imagem = MainActivity.getImagem()  ;
        }




    }
}
